package icpc.nw2019;

import java.util.Scanner;

public class J {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();
        scan.nextLine();

        int[][] matr = new int[n][n];
        for (int i = 0; i < n; i++) {
            String s = scan.nextLine();
            for (int j = 0; j < n; j++) {
                matr[i][j] = s.charAt(j) - '0';
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (matr[i][j] != 0) {
                    System.out.print(1);
                    for (int k = j + 1; k < n; k++) {
                        matr[i][k] -= matr[j][k];
                        if (matr[i][k] < 0) {
                            matr[i][k] += 10;
                        }
                    }
                } else {
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }
}
