package icpc.nw2019;

import java.util.Scanner;

public class A {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int n = scan.nextInt();

        System.out.println(2 * (int)Math.ceil((double)(n - b) / (b - a)) + 1);
    }
}
