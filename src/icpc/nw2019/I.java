package icpc.nw2019;

import java.util.Scanner;

public class I {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int n = scan.nextInt();

        long xl = Long.MAX_VALUE, xr = Long.MIN_VALUE, yl = Long.MAX_VALUE, yr = Long.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            long x = scan.nextLong();
            long y = scan.nextLong();
            long h = scan.nextLong();

            xl = Math.min(xl, x - h);
            xr = Math.max(xr, x + h);
            yl = Math.min(yl, y - h);
            yr = Math.max(yr, y + h);
        }

        System.out.print("" + (long) Math.ceil(1.0D * (xl + xr) / 2));
        System.out.print(" " + (long) Math.ceil(1.0D * (yl + yr) / 2));
        System.out.println(" " + (long) Math.ceil(1.0D * Math.max(xr - xl, yr - yl) / 2));
    }
}
