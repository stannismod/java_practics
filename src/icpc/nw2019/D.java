package icpc.nw2019;

import java.util.Scanner;

public class D {

    private static final int MOD = 998_244_353;

    /*
    private static int R(int n, int k) {
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += Math.pow(k, Math.ceil(i / 2) + Math.ceil((n - i) / 2));
        return sum;
    }
    */

    private static long D(int n, int k) {
        long[] powers = new long[n + 1];
        long[] polydroms = new long[n + 1];
        long[] total = new long[n + 1];

        powers[0] = 1;

        long all = 0;
        for (int i = 1; i <= n; i++) {
            powers[i] = multiply(powers[i - 1], k);

            total[i] += i % 2 == 1 ? i * powers[(i + 1) / 2] : multiply(i / 2, powers[i / 2] + powers[i / 2 + 1]);
            polydroms[i] += total[i];

            // R(i, k)

            for (int j = 2; i * j <= n; j++) {
                total[i * j] -= multiply(polydroms[i], j - 1);
                polydroms[i * j] -= polydroms[i];
            }

            all += total[i];
        }
        return all;
    }

    private static long multiply(final long a, final long b) {
        return (a * b) % MOD;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int k = scan.nextInt();

        System.out.println(D(n, k) % MOD);
    }
}
