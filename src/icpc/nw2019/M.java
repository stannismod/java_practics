package icpc.nw2019;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class M {

    private static int count(int[] a) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;

        for (int j = a.length - 1; j >= 0; j--) {
            for (int i = 0; i < j; i++) {
                sum += map.getOrDefault(2 * a[j] - a[i], 0);
            }
            map.merge(a[j], 1, Integer::sum);
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for (int i = 0; i < n; i++) {
            int k = scan.nextInt();
            int[] array = new int[k];
            for (int j = 0; j < k; j++) {
                array[j] = scan.nextInt();
            }

            System.out.println(count(array));
        }
    }
}
