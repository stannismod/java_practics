package icpc.nw2019;

import java.util.Scanner;

public class B {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int l = -710 * 25_000;
        for (int i = 0; i < n; i++) {
            System.out.println(l);
            l += 710;
        }
    }
}
