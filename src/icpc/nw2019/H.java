package icpc.nw2019;

import java.util.Scanner;

public class H {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int[] transactions = new int[n];

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++) {
            transactions[i] = scan.nextInt();
            if (transactions[i] > max) {
                max = transactions[i];
            }
        }

        int q = scan.nextInt();

        for (int i = 0; i < q; i++) {
            int t = scan.nextInt();

            if (t < max) {
                System.out.println("Impossible");
                continue;
            }

            int counter = 1;
            int sum = 0;
            for (int j = 0; j < n; j++) {
                if (sum + transactions[j] > t) {
                    counter++;
                    sum = 0;
                    j--;
                    continue;
                }

                sum += transactions[j];
            }

            System.out.println(counter);
        }
    }
}
