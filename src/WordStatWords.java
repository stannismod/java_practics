import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class WordStatWords {

    public static void main(String[] args) {
        if (args.length < 2) {
            return;
        }

        Map<String, Integer> stat = new TreeMap<>();

        try (Scanner scan = new Scanner(new InputStreamReader(new FileInputStream(new File(args[0])), "utf8"))) {
            while (scan.hasNext()) {
                String s;
                if ((s = scan.nextWord()) != null) {
                    stat.merge(s.toLowerCase(), 1, Integer::sum);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the input file " + args[0]);
        } catch (IOException e) {
            System.err.println("Error while reading from input file with Scanner " + args[0] + ": " + e.getMessage());
            e.printStackTrace();
        }

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(args[1])), "utf8"))) {
            for (Map.Entry<String, Integer> entry : stat.entrySet()) {
                writer.write(entry.getKey() + " " + entry.getValue());
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Error while writing to output file " + args[1] + ":");
            e.printStackTrace();
        }
    }
}
