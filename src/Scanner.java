import java.io.*;
import java.util.NoSuchElementException;

public class Scanner implements AutoCloseable {

    private BufferedReader reader;
    private String s;
    private boolean canRead;
    private int vec;
    private int intValue;

    public Scanner(String s) {
        this.canRead = false;
        this.s = s;
    }

    public Scanner(Reader reader) {
        this.reader = new BufferedReader(reader);
        this.canRead = true;
    }

    public Scanner(Reader reader, int buffer) {
        this.reader = new BufferedReader(reader, buffer);
        this.canRead = true;
    }

    public Scanner(File file) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(file));
        this.canRead = true;
    }

    public boolean hasNextLine() {
        return canRead;
    }

    private void readNext() throws IOException {
        if (!hasNextLine()) {
            return;
        }

        s = reader.readLine();
        vec = 0;
        canRead = s != null;
    }

    public boolean isInLineEnd() {
        if (isEmpty()) {
            return true;
        }
        return vec >= s.length();
    }

    public String nextLine() throws IOException {
        if (hasNextLine()) {
            readNext();
        }
        return currentLine();
    }

    public int nextInt() throws IOException {
        if (!hasNextInt()) {
            throw new NoSuchElementException();
        }
        return Integer.parseInt(findNext());
    }

    public boolean isEmpty() {
        return s == null;
    }

    public String currentLine() throws IOException {
        if (isEmpty() || isInLineEnd()) {
            readNext();
        }
        return s;
    }

    private static boolean isWordPart(char c) {
        return Character.isLetter(c) || Character.getType(c) == Character.DASH_PUNCTUATION || c == '\'';
    }

    public String nextWord() throws IOException {
        while (hasNextLine()) {
            String input = currentLine();

            if (input == null) {
                return null;
            }

            while (vec < input.length() && !isWordPart(input.charAt(vec))) {
                vec++;
            }

            int start = vec;

            while (vec < input.length() && isWordPart(input.charAt(vec))) {
                vec++;
            }

            if (vec != start) {
                return input.substring(start, vec);
            }
        }

        return null;
    }

    public boolean hasNextWord() throws IOException {
        if (!hasNext()) {
            return false;
        }

        while (hasNextLine()) {
            String input = currentLine();

            if (input == null) {
                return false;
            }

            int i = vec;

            while (i < input.length() && !isWordPart(input.charAt(i))) {
                i++;
            }

            if (isWordPart(input.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Find next consequence
     * @return Found consequence
     */

    private String findNext() throws IOException {
        while (hasNextLine()) {
            String input = currentLine();

            if (input == null) {
                return null;
            }

            while (vec < input.length() && Character.isWhitespace(input.charAt(vec))) {
                vec++;
            }

            int start = vec;

            while (vec < input.length() && !Character.isWhitespace(input.charAt(vec))) {
                vec++;
            }

            if (vec != start) {
                return input.substring(start, vec);
            }
        }

        return null;
    }

    public boolean hasNextInt() throws IOException {
        if (!hasNext()) {
            return false;
        }
        try {
            String s = catchNext();
            if (s == null) {
                return false;
            }
            intValue = Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean hasNext() throws IOException {
        return catchNext() != null;
    }

    private String catchNext() throws IOException {
        while (hasNextLine()) {
            String input = currentLine();

            if (input == null) {
                return null;
            }

            int i = vec;

            while (i < input.length() && Character.isWhitespace(input.charAt(i))) {
                i++;
            }

            int start = i;

            while (i < input.length() && !Character.isWhitespace(input.charAt(i))) {
                i++;
            }

            if (i != start) {
                return input.substring(start, i);
            }
        }

        return null;
    }

    @Override
    public void close() {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException e) {
            System.err.println("Cannot close Scanner: " + e.getMessage());
        }
    }
}
