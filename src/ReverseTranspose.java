import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReverseTranspose {

    private static int[] addToArray(int[] array, int num) {
        if (array[0] >= array.length) {
            array = Arrays.copyOf(array, 2 * array.length);
        }
        //System.err.println("Size: " + array.length + ", index: " + array[0]);
        array[array[0]] = num;
        array[0]++;
        //System.err.println("Size: " + array.length + ", index: " + array[0]);
        return array;
    }
	
	public static void main(String[] args) {
		List<int[]> list = new ArrayList<>(1000_000);

		try (Scanner scan = new Scanner(new BufferedReader(new InputStreamReader(System.in)))) {
			while (scan.hasNextLine()) {
				int row = 0;
				while (scan.hasNextInt()) {
					int buf = scan.nextInt();
					if (row == list.size()) {
					    int[] arr = new int[16];
					    arr[0] = 1;
						list.add(arr);
					}
					list.set(row, addToArray(list.get(row), buf));
					row++;
					if (scan.isInLineEnd()) {
						break;
					}
				}
			}
		} catch (IOException e) {
			System.err.println("I/O error");
		}

		for (int i = 0; i < list.size(); i++) {
			for (int j = 1; j < list.get(i)[0]; j++) {
				System.out.print(list.get(i)[j] + (j == list.get(i).length - 1 ? "" : " "));
			}
			System.out.println();
		}
	}
}