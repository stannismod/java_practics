package expression.parser;

public class Abs extends UnaryOperation {

    public Abs(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        return Math.abs(x);
    }
}
