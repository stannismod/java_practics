package expression.parser;

public class Negative extends UnaryOperation {

    public Negative(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        return -x;
    }
}
