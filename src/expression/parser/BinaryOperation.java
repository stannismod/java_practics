package expression.parser;

public abstract class BinaryOperation implements CommonExpression {

    private TripleExpression par1;
    private TripleExpression par2;

    public BinaryOperation(TripleExpression par1, TripleExpression par2) {
        this.par1 = par1;
        this.par2 = par2;
    }

    /*
    abstract char getOperation();

    @Override
    public String toString() {
        return "(" + par1 + ' ' + getOperation() + ' ' + par2 + ')';
    }
    */

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BinaryOperation)) {
            return false;
        }

        BinaryOperation bo = (BinaryOperation) o;

        return bo.par1.equals(this.par1) && bo.par2.equals(this.par2) && bo.getClass() == this.getClass();
    }

    @Override
    public int evaluate(int x, int y, int z) {
        return evaluate(par1.evaluate(x, y, z), par2.evaluate(x, y, z));
    }

    protected abstract int evaluate(int x, int y);

    @Override
    public int hashCode() {
        return 13 * par1.hashCode() + 17 * par2.hashCode() + 19 * getClass().hashCode();
    }
}
