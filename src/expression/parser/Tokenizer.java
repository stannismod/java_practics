package expression.parser;

import expression.parser.exception.*;

import java.util.Map;
import java.util.Set;

@Deprecated
public class Tokenizer {

    private static Map<String, Token> IDENTIFIERS = Map.of(
            "x", Token.VARIABLE,
            "y", Token.VARIABLE,
            "z", Token.VARIABLE
    );
    private static Set<Token> BINARY_OPERATIONS = Set.of(
            Token.ADD,
            Token.SUB,
            Token.MUL,
            Token.DIV,
            Token.LEFT_SHIFT,
            Token.RIGHT_SHIFT
    );
    private static Set<Token> OPERATIONS = Set.of(
            Token.ADD,
            Token.SUB,
            Token.MUL,
            Token.DIV,
            Token.LEFT_SHIFT,
            Token.RIGHT_SHIFT,

            Token.NEG
    );

    private String expression;
    private Token curToken;
    private int vec;
    // For braces
    private int balance;
    // For variables
    private String name;
    // For constants
    private int value;

    public Tokenizer(String expression) {
        this.expression = expression;
        this.curToken = Token.BEGIN;
        this.balance = 0;
    }

    public int getVec() {
        return vec;
    }

    public Token getCurrentToken() {
        return curToken;
    }

    public Token getNextToken() throws ParsingException {
        nextToken();
        return curToken;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getExpression() {
        return expression;
    }

    public int length() {
        return expression.length();
    }

    private void skipWhitespace() {
        while (vec < length() && Character.isWhitespace(expression.charAt(vec))) {
            vec++;
        }
    }

    private boolean isNumberPart(char ch) {
        return Character.isDigit(ch) || ch == '.' || ch == 'e';
    }

    private boolean isIdentifierPart(char ch) {
        return Character.isLetterOrDigit(ch) || ch == '_';
    }

    // TODO Rewrite with IllegalConstantException

    private String parseNumber() {
        int start = vec;
        while (vec < length() && isNumberPart(expression.charAt(vec))) {
            vec++;
        }
        vec--;
        return expression.substring(start, vec + 1);
    }

    private String parseIdentifier() throws UnknownSymbolException {
        if (!Character.isLetter(expression.charAt(vec))) {
            throw new UnknownSymbolException(expression, vec);
        }
        int start = vec;
        while (vec < length() && isIdentifierPart(expression.charAt(vec))) {
            vec++;
        }
        vec--;
        return expression.substring(start, vec + 1);
    }

    private void checkForOperand() throws MissingOperandException {
        if (OPERATIONS.contains(curToken) || curToken == Token.OPEN_BRACE || curToken == Token.BEGIN) {
            throw new MissingOperandException(expression, vec);
        }
    }

    private void checkForOperation() throws MissingOperationException {
        if (curToken == Token.CLOSE_BRACE || curToken == Token.VARIABLE || curToken == Token.CONST) {
            throw new MissingOperationException(expression, vec);
        }
    }

    private boolean checkCurToken(Token ... values) {
        for (Token value : values) {
            if (curToken == value) {
                return true;
            }
        }
        return false;
    }

    private int getConst(String source) throws IncorrectConstException {
        try {
            return Integer.parseInt(source);
        } catch (NumberFormatException e) {
            throw new IncorrectConstException(source);
        }
    }

    public void nextToken() throws ParsingException {
        skipWhitespace();
        if (vec >= length()) {
            curToken = Token.END;
            return;
        }
        char ch = expression.charAt(vec);
        switch (ch) {
            case '-':
                if (checkCurToken(Token.VARIABLE, Token.CONST, Token.CLOSE_BRACE)) {
                    curToken = Token.SUB;
                } else {
                    if (vec + 1 >= expression.length()) {
                        throw new MissingOperandException(expression, vec + 1);
                    }
                    if (Character.isDigit(expression.charAt(vec + 1))) {
                        vec++;
                        String temp = parseNumber();
                        value = getConst("-" + temp);
                        curToken = Token.CONST;
                    } else {
                        curToken = Token.NEG;
                    }
                }
                break;
            case '+':
                checkForOperand();
                curToken = Token.ADD;
                break;
            case '*':
                checkForOperand();
                curToken = Token.MUL;
                break;
            case '/':
                checkForOperand();
                curToken = Token.DIV;
                break;
            case '(':
                if (checkCurToken(Token.CLOSE_BRACE, Token.CONST, Token.VARIABLE)) {
                    throw new OddOpeningParenthesisException(expression, vec + 1);
                }
                balance++;
                curToken = Token.OPEN_BRACE;
                break;
            case ')':
                if (BINARY_OPERATIONS.contains(curToken) || curToken == Token.OPEN_BRACE) {
                    throw new MissingOperandException(expression, vec);
                }
                balance--;
                if (balance < 0) {
                    throw new OddClosingParenthesisException(expression, vec);
                }
                curToken = Token.CLOSE_BRACE;
                break;
            case '<':
                if (expression.charAt(vec + 1) == '<') {
                    vec++;
                    curToken = Token.LEFT_SHIFT;
                } else {
                    throw new IncorrectExpressionException(expression, vec, 1);
                }
                break;
            case '>':
                if (expression.charAt(vec + 1) == '>') {
                    vec++;
                    curToken = Token.RIGHT_SHIFT;
                } else {
                    throw new IncorrectExpressionException(expression, vec, 1);
                }
                break;
            default:
                if (Character.isDigit(ch)) {
                    checkForOperation();
                    value = getConst(parseNumber());
                    curToken = Token.CONST;
                } else {
                    String idn = parseIdentifier();
                    if (!IDENTIFIERS.containsKey(idn)) {
                        throw new UnknownIdentifierException(idn, expression, vec);
                    }
                    name = idn;

                    curToken = IDENTIFIERS.get(idn);
                }
                break;
        }
        vec++;
    }
}
