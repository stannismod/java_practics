package expression.parser;

public class Subtract extends BinaryOperation{

    public Subtract(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) {
        return x - y;
    }
}
