package expression.parser;

@Deprecated
public enum Token {
    VARIABLE,
    CONST,

    ADD,
    SUB,
    MUL,
    DIV,
    NEG,

    LEFT_SHIFT,
    RIGHT_SHIFT,

    OPEN_BRACE,
    CLOSE_BRACE,

    BEGIN,
    END
}
