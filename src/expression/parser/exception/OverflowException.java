package expression.parser.exception;

public class OverflowException extends EvaluatingException {

    public OverflowException(String message) {
        super("Overflow in " + message);
    }
}
