package expression.parser.exception;

public class OddClosingParenthesisException extends ParsingException {

    public OddClosingParenthesisException(String expression, int pos) {
        super("Odd closing brace", expression, pos, 1);
    }
}
