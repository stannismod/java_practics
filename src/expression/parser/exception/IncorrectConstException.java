package expression.parser.exception;

public class IncorrectConstException extends EvaluatingException {

    public IncorrectConstException(String value) {
        super("Incorrect const: " + value);
    }
}
