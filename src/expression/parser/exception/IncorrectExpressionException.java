package expression.parser.exception;

public class IncorrectExpressionException extends ParsingException {

    public IncorrectExpressionException(String expression, int pos, int size) {
        super("Incorrect expression", expression, pos, size);
    }

    public IncorrectExpressionException(String message, String expression, int pos, int size) {
        super(message, expression, pos, size);
    }
}
