package expression.parser.exception;

public class EvaluatingException extends RuntimeException {

    public EvaluatingException(String message) {
        super(message);
    }
}
