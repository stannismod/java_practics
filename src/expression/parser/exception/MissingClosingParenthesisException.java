package expression.parser.exception;

public class MissingClosingParenthesisException extends ParsingException {

    public MissingClosingParenthesisException(String expression, int pos) {
        super("Missing closing brace", expression, pos, 1);
    }
}
