package expression.parser.exception;

public class MissingOperandException extends ParsingException {

    public MissingOperandException(String expression, int pos) {
        super("Missing operand", expression, pos, 1);
    }
}
