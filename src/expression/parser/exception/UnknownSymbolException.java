package expression.parser.exception;

public class UnknownSymbolException extends ParsingException {

    public UnknownSymbolException(String expression, int pos) {
        super("Unknown symbol: '" + expression.charAt(pos) + "' at pos " + pos, expression, pos, 1);
    }
}
