package expression.parser.exception;

public class OddOpeningParenthesisException extends ParsingException {

    public OddOpeningParenthesisException(String expression, int pos) {
        super("Odd opening brace", expression, pos, 1);
    }
}
