package expression.parser.exception;

public class UnknownIdentifierException extends ParsingException {

    public UnknownIdentifierException(String name, String expression, int pos) {
        super("Unknown identifier: " + name, expression, pos, name.length());
    }
}
