package expression.parser.exception;

public class MissingOperationException extends ParsingException {

    public MissingOperationException(String expression, int pos) {
        super("Missing operation", expression, pos, 1);
    }
}
