package expression.parser.exception;

public class ParsingException extends Exception {

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, String expression, int pos, int size) {
        super(message + "\n" + expression + "\n" + getUnderlineString(pos, size));
    }

    private static String getUnderlineString(int pos, int size) {
        return " ".repeat(pos > 0 ? pos - 1 : 0) + "~".repeat(size);
    }
}
