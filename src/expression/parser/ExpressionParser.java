package expression.parser;

import expression.parser.exception.ParsingException;

import java.util.Set;

public class ExpressionParser implements Parser {

    private ExpressionSource source;

    private static Set<String> VARIABLES = Set.of(
            "x",
            "y",
            "z"
    );

    private CommonExpression priority3() throws ParsingException {
        CommonExpression res = null;

        if (Character.isDigit(source.current())) {
            return parseConst("");
        } else if (test('-')) {                    // CONST
            if (Character.isDigit(source.current())) {
                return parseConst("-");
            } else {
                return new Negative(priority3());
            }
        } else if (Character.isLetter(source.current())) {  // IDENTIFIER
            String idn = source.nextIdentifier();
            if (VARIABLES.contains(idn)) {
                res = new Variable(idn);
            } else {
                skipWhitespace();
                switch (idn) {
                    case "abs":   // Abs
                        res = new Abs(priority3());
                        break;
                    case "square":   // Square
                        res = new Square(priority3());
                        break;
                    case "reverse":   // Reverse
                        res = new Reverse(priority3());
                        break;
                    case "digits":   // Digits
                        return new Digits(priority3());
                    default:

                }
            }
        } else if (test('(')) {
            res = priority0();
            test(')');
        }
        skipWhitespace();
        return res;
    }

    private Const parseConst(String prefix) {
        Const val = new Const(Integer.parseInt(prefix + source.nextConst()));
        skipWhitespace();
        return val;
    }

    private CommonExpression priority2() throws ParsingException {
        CommonExpression res = priority3();
        while (true) {
            if (test('*')) {
                res = new Multiply(res, priority3());
            } else if (test('/')) {
                res = new Divide(res, priority3());
            } else {
                return res;
            }
        }
    }

    private CommonExpression priority1() throws ParsingException {
        CommonExpression res = priority2();
        while (true) {
            if (test('+')) {
                res = new Add(res, priority2());
            } else if (test('-')) {
                res = new Subtract(res, priority2());
            } else {
                return res;
            }
        }
    }

    private CommonExpression priority0() throws ParsingException {
        CommonExpression res = priority1();
        while (true) {
            if (test('<')) {
                if (test('<')) {
                    res = new LeftShift(res, priority1());
                }
            } else if (test('>')) {
                if (test('>')) {
                    res = new RightShift(res, priority1());
                }
            } else {
                return res;
            }
        }
    }

    private void nextToken() {
        nextChar();
        skipWhitespace();
    }

    private void nextChar() {
        if (source.hasNext()) {
            source.next();
        }
    }

    private void skipWhitespace() {
        while (source.hasNext() && Character.isWhitespace(source.current())) {
            source.next();
        }
    }

    protected boolean test(char expected) {
        if (source.current() == expected) {
            nextToken();
            return true;
        }
        return false;
    }

    @Override
    public CommonExpression parse(String expression) {
        this.source = new StringSource(expression);
        skipWhitespace();
        try {
            return priority0();
        } catch (ParsingException e) {
            System.err.println("Parsing error:");
            System.err.println(e.getMessage());
            return null;
        }
    }
}
