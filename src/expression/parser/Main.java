package expression.parser;

public class Main {

    public static void main(String[] args) {
        TripleExpression expr = new ExpressionParser().parse("x + y + z * (2 - 3)  *   0");
        if (expr != null) {
            System.out.println("Result: " + expr.evaluate(1, 1, 1));
        }
    }
}
