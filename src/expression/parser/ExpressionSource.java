package expression.parser;

import expression.parser.exception.ParsingException;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public interface ExpressionSource {
    boolean hasNext();
    char next();
    char current();
    String nextIdentifier();
    String nextConst();
    ParsingException error(final String message);
}
