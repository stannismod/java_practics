package expression.parser;

public class Reverse extends UnaryOperation {

    public Reverse(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        int rev = 0;
        while (x != 0) {
            rev = rev * 10 + (x % 10);
            x = x / 10;
        }
        return rev;
    }
}
