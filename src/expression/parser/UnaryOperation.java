package expression.parser;

public abstract class UnaryOperation implements CommonExpression {

    private TripleExpression operand;

    public UnaryOperation(TripleExpression operand) {
        this.operand = operand;
    }

    protected abstract int evaluate(int x);

    @Override
    public int evaluate(int x, int y, int z) {
        return evaluate(operand.evaluate(x, y, z));
    }
}
