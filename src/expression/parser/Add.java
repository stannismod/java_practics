package expression.parser;

public class Add extends BinaryOperation {

    public Add(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) {
        return x + y;
    }
}
