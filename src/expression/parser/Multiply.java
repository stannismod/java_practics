package expression.parser;

public class Multiply extends BinaryOperation {

    public Multiply(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) {
        return x * y;
    }
}
