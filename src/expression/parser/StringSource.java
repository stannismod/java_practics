package expression.parser;

import expression.parser.exception.IncorrectExpressionException;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public class StringSource implements ExpressionSource {

    private final String data;
    private int pos;

    public StringSource(final String data) {
        this.data = data;
    }

    @Override
    public boolean hasNext() {
        return pos < data.length();
    }

    @Override
    public char next() {
        return data.charAt(pos++);
    }

    public char current() {
        return hasNext() ? data.charAt(pos) : '\0';
    }

    @Override
    public String nextIdentifier() {
        int start = pos;
        while (Character.isLetterOrDigit(current())) {
            next();
        }
        return data.substring(start, pos);

    }

    @Override
    public String nextConst() {
        if (hasNext() && Character.isDigit(current())) {
            int start = pos;
            while (hasNext() && Character.isDigit(current())) {
                next();
            }
            return data.substring(start, pos);
        }
        return null;
    }


    @Override
    public IncorrectExpressionException error(final String message) {
        return new IncorrectExpressionException(message, data, pos, 1);
    }
}
