package expression.parser;

public class Square extends UnaryOperation {

    public Square(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        return x * x;
    }
}
