package expression.exceptions;

import expression.exceptions.exception.EvaluatingException;

public abstract class CheckedUnaryOperation implements CommonExpression {

    protected TripleExpression operand;

    public CheckedUnaryOperation(TripleExpression operand) {
        this.operand = operand;
    }

    protected abstract int evaluate(int x) throws EvaluatingException;

    @Override
    public int evaluate(int x, int y, int z) throws EvaluatingException {
        return evaluate(operand.evaluate(x, y, z));
    }
}
