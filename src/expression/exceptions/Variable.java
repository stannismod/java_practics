package expression.exceptions;

import expression.exceptions.exception.UnknownIdentifierException;

public class Variable implements CommonExpression {

    private final String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public int evaluate(int x, int y, int z) {
        switch (name) {
            case "x":
                return x;
            case "y":
                return y;
            case "z":
                return z;
            default:
                throw new UnknownIdentifierException(name);
        }
    }
}
