package expression.exceptions;

import expression.exceptions.exception.OverflowException;

public class Abs extends CheckedUnaryOperation {

    public Abs(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        if (x == Integer.MIN_VALUE) {
            throw new OverflowException("abs");
        }
        return Math.abs(x);
    }
}
