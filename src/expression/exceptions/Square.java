package expression.exceptions;

public class Square extends CheckedUnaryOperation {

    public Square(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        return x * x;
    }
}
