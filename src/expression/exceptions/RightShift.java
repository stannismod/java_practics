package expression.exceptions;

import expression.parser.exception.EvaluatingException;

public class RightShift extends CheckedBinaryOperation {

    public RightShift(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) throws EvaluatingException {
        if (y < 0) {
            throw new IllegalArgumentException("can't shift by negative count");
        }
        return x >> y;
    }
}
