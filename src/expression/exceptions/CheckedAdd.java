package expression.exceptions;

import expression.exceptions.exception.OverflowException;
import expression.exceptions.exception.UnderflowException;
import expression.parser.exception.EvaluatingException;

public class CheckedAdd extends CheckedBinaryOperation {

    public CheckedAdd(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) throws EvaluatingException {
        if (x > 0 && Integer.MAX_VALUE - x < y) {
            throw new OverflowException("add");
        } else if (x < 0 && Integer.MIN_VALUE - x > y) {
            throw new UnderflowException("add");
        }
        return x + y;
    }
}
