package expression.exceptions;

import expression.exceptions.exception.OverflowException;
import expression.exceptions.exception.UnderflowException;
import expression.parser.exception.EvaluatingException;

public class CheckedSubtract extends CheckedBinaryOperation {

    public CheckedSubtract(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) throws EvaluatingException {
        if (y < 0 && Integer.MAX_VALUE + y < x) {
            throw new OverflowException("subtract");
        } else if (y > 0 && Integer.MIN_VALUE + y > x) {
            throw new UnderflowException("subtract");
        }
        return x - y;
    }
}
