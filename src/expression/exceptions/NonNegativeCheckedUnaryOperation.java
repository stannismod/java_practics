package expression.exceptions;

public abstract class NonNegativeCheckedUnaryOperation extends CheckedUnaryOperation {

    public NonNegativeCheckedUnaryOperation(TripleExpression operand) {
        super(operand);
    }

    @Override
    public int evaluate(int x, int y, int z) throws IllegalArgumentException {
        int arg = operand.evaluate(x, y, z);
        if (arg < 0) {
            throw new IllegalArgumentException("Argument can't be negative!");
        }
        return super.evaluate(x, y, z);
    }
}
