package expression.exceptions;

import expression.exceptions.exception.IncorrectConstException;
import expression.exceptions.exception.ParsingException;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public interface ExpressionSource {

    boolean hasNext();

    char next();

    char current();

    String nextIdentifier();

    String nextConst() throws IncorrectConstException;

    int getPos();

    String getExpression();

    ParsingException error(final String message);
}
