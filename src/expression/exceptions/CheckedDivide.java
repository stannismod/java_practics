package expression.exceptions;

import expression.exceptions.exception.OverflowException;
import expression.parser.exception.DivisionByZeroException;
import expression.parser.exception.EvaluatingException;

public class CheckedDivide extends CheckedBinaryOperation {

    public CheckedDivide(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) throws EvaluatingException {
        if (y == 0) {
            throw new DivisionByZeroException();
        }
        if (x == Integer.MIN_VALUE && y == -1) {
            throw new OverflowException("divide");
        }
        return x / y;
    }
}
