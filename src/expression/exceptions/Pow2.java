package expression.exceptions;

import expression.exceptions.exception.EvaluatingException;
import expression.exceptions.exception.OverflowException;

public class Pow2 extends NonNegativeCheckedUnaryOperation {

    public Pow2(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) throws EvaluatingException {
        if (x >= 31) {
            throw new OverflowException("pow2");
        }
        return (int) Math.pow(2, x);
    }
}
