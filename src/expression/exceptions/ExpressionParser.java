package expression.exceptions;

import expression.exceptions.exception.*;

public class ExpressionParser implements Parser {

    private ExpressionSource source;

    private CommonExpression priority3() throws ParsingException {
        CommonExpression res;
        if (Character.isDigit(source.current())) {
            return parseConst("");
        } else if (test('-')) {                    // CONST
            if (Character.isDigit(source.current())) {
                return parseConst("-");
            } else {
                return new CheckedNegate(priority3());
            }
        } else if (Character.isLetter(source.current())) {  // IDENTIFIER
            String idn = source.nextIdentifier();
            skipWhitespace();
            switch (idn) {
                case "abs":
                    res = new Abs(priority3());
                    break;
                case "square":
                    res = new Square(priority3());
                    break;
                case "reverse":
                    res = new Reverse(priority3());
                    break;
                case "digits":
                    return new Digits(priority3());
                case "pow2":
                    return new Pow2(priority3());
                case "log2":
                    return new Log2(priority3());
                case "x":
                case "y":
                case "z":
                    res = new Variable(idn);
                    break;
                default:
                    throw new UnknownIdentifierException(idn);
            }
        } else if (test('(')) {
            res = priority0();
            if (!test(')')) {
                throw new MissingClosingParenthesisException(source);
            }
        } else {
            throw new MissingOperandException(source);
        }
        skipWhitespace();
        return res;
    }

    private CheckedConst parseConst(String prefix) throws IncorrectConstException {
        String str = prefix + source.nextConst();
        CheckedConst val;
        try {
            val = new CheckedConst(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            throw new IncorrectConstException(prefix, source);
        }
        skipWhitespace();
        return val;
    }

    private CommonExpression priority2() throws ParsingException {
        CommonExpression res = priority3();
        while (true) {
            if (test('*')) {
                res = new CheckedMultiply(res, priority3());
            } else if (test('/')) {
                res = new CheckedDivide(res, priority3());
            } else {
                return res;
            }
        }
    }

    private CommonExpression priority1() throws ParsingException {
        CommonExpression res = priority2();
        while (true) {
            if (test('+')) {
                res = new CheckedAdd(res, priority2());
            } else if (test('-')) {
                res = new CheckedSubtract(res, priority2());
            } else {
                return res;
            }
        }
    }

    private CommonExpression priority0() throws ParsingException {
        CommonExpression res = priority1();
        while (true) {
            if (test('<')) {
                if (test('<')) {
                    res = new LeftShift(res, priority1());
                } else {
                    throw new UnknownOperationException("<", source);
                }
            } else if (test('>')) {
                if (test('>')) {
                    res = new RightShift(res, priority1());
                } else {
                    throw new UnknownOperationException(">", source);
                }
            } else {
                return res;
            }
        }
    }

    private void nextToken() {
        nextChar();
        skipWhitespace();
    }

    private void nextChar() {
        if (source.hasNext()) {
            source.next();
        }
    }

    private void skipWhitespace() {
        while (source.hasNext() && Character.isWhitespace(source.current())) {
            source.next();
        }
    }

    protected boolean test(char expected) {
        if (source.current() == expected) {
            nextToken();
            return true;
        }
        return false;
    }

    @Override
    public CommonExpression parse(String expression) throws ParsingException {
        this.source = new StringSource(expression);
        skipWhitespace();
        CommonExpression expr = priority0();
        if (source.hasNext()) {
            throw new IncorrectExpressionException("Expression wasn't be parsed fully", source, 1);
        }
        return expr;
    }
}
