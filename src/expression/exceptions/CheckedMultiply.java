package expression.exceptions;

import expression.exceptions.exception.OverflowException;
import expression.exceptions.exception.UnderflowException;
import expression.parser.exception.EvaluatingException;

public class CheckedMultiply extends CheckedBinaryOperation {

    public CheckedMultiply(TripleExpression par1, TripleExpression par2) {
        super(par1, par2);
    }

    @Override
    protected int evaluate(int x, int y) throws EvaluatingException {
        if (x != 0) {
            if (x > 0) {
                if (y > 0) {
                    if (Integer.MAX_VALUE / y < x) {
                        throw new OverflowException("multiply");
                    }
                } else {
                    if (Integer.MIN_VALUE / x > y) {
                        throw new UnderflowException("multiply");
                    }
                }
            } else {
                if (y > 0) {
                    if (Integer.MIN_VALUE / y > x) {
                        throw new OverflowException("multiply");
                    }
                } else {
                    if (Integer.MAX_VALUE / x > y) {
                        throw new UnderflowException("multiply");
                    }
                }
            }
        }
        return x * y;
    }
}
