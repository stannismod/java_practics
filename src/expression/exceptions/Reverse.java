package expression.exceptions;

import expression.exceptions.exception.EvaluatingException;
import expression.exceptions.exception.OverflowException;

public class Reverse extends CheckedUnaryOperation {

    public Reverse(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) throws EvaluatingException {
        String s = String.valueOf(x);
        StringBuilder builder = new StringBuilder(s);
        int value;
        try {
            value = Integer.parseInt(builder.reverse().toString());
        } catch (NumberFormatException e) {
            throw new OverflowException("square");
        }
        return value;
    }
}
