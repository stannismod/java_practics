package expression.exceptions;

import expression.exceptions.exception.ParsingException;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public interface Parser {

    TripleExpression parse(String expression) throws ParsingException;
}
