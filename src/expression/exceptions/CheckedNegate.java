package expression.exceptions;

import expression.exceptions.exception.EvaluatingException;
import expression.exceptions.exception.OverflowException;

public class CheckedNegate extends CheckedUnaryOperation {

    public CheckedNegate(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) throws EvaluatingException {
        if (x == Integer.MIN_VALUE) {
            throw new OverflowException("negate");
        }
        return -x;
    }
}
