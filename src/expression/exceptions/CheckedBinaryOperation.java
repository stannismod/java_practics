package expression.exceptions;

import expression.parser.exception.EvaluatingException;

public abstract class CheckedBinaryOperation implements CommonExpression {

    private TripleExpression par1;
    private TripleExpression par2;

    public CheckedBinaryOperation(TripleExpression par1, TripleExpression par2) {
        this.par1 = par1;
        this.par2 = par2;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CheckedBinaryOperation)) {
            return false;
        }

        CheckedBinaryOperation bo = (CheckedBinaryOperation) o;

        return bo.par1.equals(this.par1) && bo.par2.equals(this.par2) && bo.getClass() == this.getClass();
    }

    @Override
    public int evaluate(int x, int y, int z) throws EvaluatingException {
        return evaluate(par1.evaluate(x, y, z), par2.evaluate(x, y, z));
    }

    protected abstract int evaluate(int x, int y) throws EvaluatingException;

    @Override
    public int hashCode() {
        return 13 * par1.hashCode() + 17 * par2.hashCode() + 19 * getClass().hashCode();
    }
}
