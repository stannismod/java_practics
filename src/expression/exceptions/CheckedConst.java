package expression.exceptions;

import expression.parser.exception.IncorrectConstException;

public class CheckedConst implements CommonExpression {

    private final int value;

    public CheckedConst(int value) {
        this.value = value;
    }

    @Override
    public int evaluate(int x, int y, int z) throws IncorrectConstException {
        return value;
    }
}
