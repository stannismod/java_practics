package expression.exceptions;

public class Digits extends CheckedUnaryOperation {

    public Digits(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        int sum = 0;
        x = Math.abs(x);
        while (x != 0) {
            sum += x % 10;
            x /= 10;
        }
        return sum;
    }
}
