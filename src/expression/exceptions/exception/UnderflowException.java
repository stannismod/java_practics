package expression.exceptions.exception;

public class UnderflowException extends EvaluatingException {

    public UnderflowException(String source) {
        super("Underflow in " + source);
    }
}
