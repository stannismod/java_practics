package expression.exceptions.exception;

public class OverflowException extends EvaluatingException {

    public OverflowException(String source) {
        super("Overflow in " + source);
    }
}
