package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class IncorrectConstException extends ParsingException {

    public IncorrectConstException(String value, ExpressionSource source) {
        super("Incorrect const: " + value, source, value.length());
    }
}
