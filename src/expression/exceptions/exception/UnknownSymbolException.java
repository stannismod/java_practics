package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class UnknownSymbolException extends ParsingException {

    public UnknownSymbolException(ExpressionSource source) {
        super("Unknown symbol: '" + source.getExpression().charAt(source.getPos()) + "' at pos " + source.getExpression(), source, 1);
    }
}
