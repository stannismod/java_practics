package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class UnknownOperationException extends ParsingException {

    public UnknownOperationException(String sym, ExpressionSource source) {
        super("Unknown operation: " + sym, source, sym.length());
    }
}
