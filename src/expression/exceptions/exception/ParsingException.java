package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class ParsingException extends Exception {

    public ParsingException(String message) {
        super(message);
    }

    public ParsingException(String message, ExpressionSource source, int size) {
        super(message + "\n" + source.getExpression() + "\n" + getUnderlineString(source.getPos(), size));
    }

    private static String getUnderlineString(int pos, int size) {
        return " ".repeat(Math.max(pos - 1, 0)) + "~".repeat(size);
    }
}
