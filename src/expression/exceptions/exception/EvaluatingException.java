package expression.exceptions.exception;

public class EvaluatingException extends RuntimeException {

    public EvaluatingException(String message) {
        super(message);
    }
}
