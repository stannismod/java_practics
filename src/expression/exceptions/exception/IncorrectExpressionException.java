package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class IncorrectExpressionException extends ParsingException {

    public IncorrectExpressionException(ExpressionSource source, int size) {
        super("Incorrect expression", source, size);
    }

    public IncorrectExpressionException(String message, ExpressionSource source, int size) {
        super(message, source, size);
    }
}
