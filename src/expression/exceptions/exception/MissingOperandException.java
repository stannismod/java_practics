package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class MissingOperandException extends ParsingException {

    public MissingOperandException(ExpressionSource source) {
        super("Missing operand", source, 1);
    }
}
