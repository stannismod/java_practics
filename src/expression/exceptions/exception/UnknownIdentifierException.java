package expression.exceptions.exception;

public class UnknownIdentifierException extends EvaluatingException {

    public UnknownIdentifierException(String name) {
        super("Unknown identifier: " + name);
    }
}
