package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class MissingClosingParenthesisException extends ParsingException {

    public MissingClosingParenthesisException(ExpressionSource source) {
        super("Missing closing brace", source, 1);
    }
}
