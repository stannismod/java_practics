package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class OddClosingParenthesisException extends ParsingException {

    public OddClosingParenthesisException(ExpressionSource source) {
        super("Odd closing brace", source, 1);
    }
}
