package expression.exceptions.exception;

import expression.exceptions.ExpressionSource;

public class MissingOperationException extends ParsingException {

    public MissingOperationException(ExpressionSource source) {
        super("Missing operation", source, 1);
    }
}
