package expression.exceptions;

public class Log2 extends NonNegativeCheckedUnaryOperation {

    public Log2(TripleExpression operand) {
        super(operand);
    }

    @Override
    protected int evaluate(int x) {
        if (x == 0) {
            throw new IllegalArgumentException("Argument of log2 can't be zero");
        }
        return (int)(Math.log(x) / Math.log(2));
    }
}
