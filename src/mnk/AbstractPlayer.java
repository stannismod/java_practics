package mnk;

public abstract class AbstractPlayer implements Player {

    private String name;

    protected AbstractPlayer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
