package mnk;

public class MNKSettings implements Settings {

    private int m;
    private int n;
    private int k;

    public MNKSettings(int m, int n, int k) {
        this.m = m;
        this.n = n;
        this.k = k;
    }

    @Override
    public Board createBoard() {
        return new MNKBoard(m, n, k);
    }
}
