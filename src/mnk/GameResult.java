package mnk;

public class GameResult {

    Pair first;
    Pair second;

    public GameResult(Pair first, Pair second) {
        this.first = first;
        this.second = second;
    }

    public String toString() {
        return "First: " + first.toString() + " | " + "Second: " + second.toString();
    }
}
