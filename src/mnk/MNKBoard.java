package mnk;

import java.util.Arrays;
import java.util.Map;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public class MNKBoard implements Board {

    private static final Map<Cell, Character> SYMBOLS = Map.of(
            Cell.X, 'X',
            Cell.O, 'O',
            Cell.E, '.'
    );

    private final Cell[][] cells;
    private Cell turn;
    private int emptyCells;
    private int k;

    private Position client;

    private class ClientBoard implements Position {

        @Override
        public boolean isValid(final Move move) {
            return 0 <= move.getRow() && move.getRow() < getHeight()
                    && 0 <= move.getColumn() && move.getColumn() < getWidth()
                    && getCell(move.getRow(), move.getColumn()) == Cell.E;
        }

        @Override
        public Cell getCell(int r, int c) {
            return cells[r][c];
        }

        @Override
        public int getWidth() {
            return cells[0].length;
        }

        @Override
        public int getHeight() {
            return cells.length;
        }
    }

    public MNKBoard(final int m, final int n, final int k) {
        this.cells = new Cell[m][n];
        this.k = k;
        for (Cell[] row : cells) {
            Arrays.fill(row, Cell.E);
        }
        this.emptyCells = m * n;
        this.turn = Cell.X;
        this.client = new ClientBoard();
    }

    @Override
    public Cell getCell() {
        return turn;
    }

    private boolean checkIndexes(int r, int c) {
        return r >= 0 && r < cells.length && c >= 0 && c < cells[0].length;
    }

    private int getInDirection(int r, int c, int vx, int vy, Cell turn) {
        int counter = -1;
        while (checkIndexes(r, c) && cells[r][c] == turn) {
            r += vy;
            c += vx;
            counter++;
        }
        return counter;
    }

    @Override
    public Position getClient() {
        return client;
    }

    private enum Direction {
        HORIZONTAL(-1, 0),
        VERTICAL(0, -1),
        MAIN_DIAG(1, 1),
        OFF_DIAG(1, -1);

        private int x1;
        private int y1;

        Direction(int x1, int y1) {
            this.x1 = x1;
            this.y1 = y1;
        }

        private boolean isWin(int r, int c, Cell turn, MNKBoard mnkBoard) {
            return 1 + mnkBoard.getInDirection(r, c, x1, y1, turn) + mnkBoard.getInDirection(r, c, -x1, -y1, turn) >= mnkBoard.k;
        }
    }

    private boolean check(int r, int c, Cell turn) {
        for (Direction dir : Direction.values()) {
            if (dir.isWin(r, c, turn, this)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Result makeMove(final Move move) {
        if (!client.isValid(move)) {
            return Result.LOSE;
        }

        cells[move.getRow()][move.getColumn()] = move.getValue();

        this.emptyCells--;

        if (check(move.getRow(), move.getColumn(), turn)) {
            return Result.WIN;
        }
        if (this.emptyCells == 0) {
            return Result.DRAW;
        }

        turn = turn == Cell.X ? Cell.O : Cell.X;
        return Result.UNKNOWN;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(" ");
        for (int i = 0; i < cells[0].length; i++) {
            sb.append(String.valueOf(i));
        }
        for (int r = 0; r < cells.length; r++) {
            sb.append("\n");
            sb.append(r);
            for (int c = 0; c < cells[0].length; c++) {
                sb.append(SYMBOLS.get(cells[r][c]));
            }
        }
        return sb.toString();
    }
}
