package mnk;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("========= Tournament =========");
        int[] scanned = Helper.getPositiveIntArray(System.out, scan, 5, "Enter m, n, k, c, numPlayers with a space as a delimiter");

        Player[] players = new Player[scanned[4]];
        for (int j = 0; j < players.length; j++) {
            players[j] = new RandomPlayer(j);
        }
        Tournament game = new Tournament(false, scanned[3], players);

        TournamentResults results = game.play(new MNKSettings(scanned[0], scanned[1], scanned[2]));

        for (Pair result : results.getSortedResults()) {
            System.out.println(result);
        }

        scan.close();
    }

}
