package mnk;

import java.util.ArrayList;
import java.util.List;

public class Tournament {

    private final int circles;
    private final List<Game> games;
    private int numPlayers;

    public Tournament(boolean log, int circles, Player[] players) {
        this.circles = circles;
        this.games = new ArrayList<>();
        this.numPlayers = players.length;
        for (int i = 0; i < numPlayers; i++) {
            for (int j = i + 1; j < numPlayers; j++) {
                games.add(new Game(log, players[i], players[j]));
            }
        }
    }

    public TournamentResults play(Settings settings) {
        List<GameResult> results = new ArrayList<>();

        for (int k = 0; k < circles; k++) {
            for (Game game : games) {
                Board board = settings.createBoard();
                results.add(game.play(board));
            }
        }

        return new TournamentResults(numPlayers, circles, results);
    }
}
