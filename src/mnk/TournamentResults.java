package mnk;

import java.util.*;

public class TournamentResults {

    private List<GameResult> results;
    private int numPlayers;
    private int circles;

    TournamentResults(int numPlayers, int circles, List<GameResult> results) {
        this.results = results;
        this.numPlayers = numPlayers;
        this.circles = circles;
    }

    public List<Pair> getCompleteResults() {
        return getResultsForCircle(circles);
    }

    // n * (n - 1) / 2 entries of GameResult = one circle

    public List<Pair> getResultsForCircle(int circles) {
        Map<String, Pair> playerResults = new HashMap<>();
        int step = 0;
        int size = numPlayers * (numPlayers - 1) / 2;
        for (int i = 0; i < circles; i++) {
            for (int j = 0; j < size; j++) {
                GameResult entry = results.get(step + j);
                merge(playerResults, entry.first);
                merge(playerResults, entry.second);
            }
            step += size;
        }
        return new ArrayList<>(playerResults.values());
    }

    public Pair getPlayerResult(Player player) {
        Pair playerResult = new Pair(player);
        for (GameResult result : results) {
            if (result.first.getPlayer() == player) {
                playerResult.score += result.first.score;
            } else if (result.second.getPlayer() == player) {
                playerResult.score += result.second.score;
            }
        }
        return playerResult;
    }

    private static void merge(Map<String, Pair> map, Pair result) {
        map.merge(result.getPlayer().getName(), result, (oldValue, newValue) -> new Pair(oldValue.score + newValue.score, newValue.getPlayer()));
    }

    public List<Pair> getSortedResults() {
        List<Pair> sortedResults = getCompleteResults();
        Collections.sort(sortedResults);
        return sortedResults;
    }
}
