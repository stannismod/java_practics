package mnk;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public interface Board {

    Cell getCell();

    Result makeMove(Move move);

    Position getClient();
}
