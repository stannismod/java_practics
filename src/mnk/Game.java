package mnk;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public class Game {

    private final boolean log;
    private final Player player1, player2;

    public Game(final boolean log, final Player player1, final Player player2) {
        this.log = log;
        this.player1 = player1;
        this.player2 = player2;
    }

    private GameResult constructResult(boolean first, Player player1, Player player2) {
        return new GameResult(new Pair(first ? 3 : 1, player1), new Pair(first ? 1 : 3, player2));
    }

    public GameResult play(Board board) {
        while (true) {
            final int result1 = move(board, player1, 1);
            if (result1 != -1) {
                return constructResult(true, player1, player2);
            }
            final int result2 = move(board, player2, 2);
            if (result2 != -1) {
                return constructResult(false, player1, player2);
            }
        }
    }

    private int move(final Board board, final Player player, final int no) {
        final Move move = player.move(board.getClient(), board.getCell());
        final Result result = board.makeMove(move);
        log("Player " + no + " move: " + move);
        log("Position:\n" + board);
        if (result == Result.WIN) {
            log("Player " + no + " won");
            return 3;
        } else if (result == Result.LOSE) {
            log("Player " + no + " lose");
            return 1;
        } else if (result == Result.DRAW) {
            log("Draw");
            return 0;
        } else {
            return -1;
        }
    }

    private void log(final String message) {
        if (log) {
            System.out.println(message);
        }
    }
}
