package mnk;

import java.io.PrintStream;
import java.util.Scanner;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public class HumanPlayer extends AbstractPlayer {

    private final PrintStream out;
    private final Scanner in;

    public HumanPlayer(String name, final PrintStream out, final Scanner in) {
        super(name);
        this.out = out;
        this.in = in;
    }

    public HumanPlayer(String name, final Scanner in) {
        this(name, System.out, in);
    }

    @Override
    public Move move(final Position position, final Cell cell) {
        while (true) {
            out.println("Position");
            out.println(position);
            out.println(cell + "'s move");
            String prediction = "Enter row and column";

            int[] scanned = Helper.getPositiveIntArray(out, in, 2, prediction);

            final Move move = new Move(scanned[0], scanned[1], cell);
            if (position.isValid(move)) {
                return move;
            }
            out.println("Move " + move + " is invalid");
        }
    }
}
