package mnk;

import java.io.PrintStream;
import java.util.Scanner;

class Helper {

    static int[] getPositiveIntArray(PrintStream out, Scanner scan, int n, String prediction) {
        int[] array = new int[n];
        int i;
        while (true) {
            out.println(prediction);
            for (i = 0; i < n; i++) {
                if ((array[i] = getPositiveInt(scan)) == -1) {
                    break;
                }
            }
            if (i != n) {
                continue;
            }
            return array;
        }
    }

    private static int getPositiveInt(Scanner scan) {
        int value = -1;
        if (scan.hasNextInt()) {
            value = scan.nextInt();
        } else {
            if (scan.hasNext()) {
                scan.nextLine();
            } else {
                System.err.println("End of input");
                System.exit(-1);
            }
        }
        return value;
    }
}
