package mnk;

import java.util.Random;

/**
 * @author Georgiy Korneev (kgeorgiy@kgeorgiy.info)
 */
public class RandomPlayer extends AbstractPlayer {

    private final Random random;

    public RandomPlayer(int index, final Random random) {
        super("RandomPlayer#" + index);
        this.random = random;
    }

    public RandomPlayer(int index) {
        this(index, new Random());
    }

    @Override
    public Move move(final Position position, final Cell cell) {
        while (true) {
            int r = random.nextInt(position.getHeight());
            int c = random.nextInt(position.getWidth());
            final Move move = new Move(r, c, cell);
            if (position.isValid(move)) {
                return move;
            }
        }
    }
}
