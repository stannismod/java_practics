package mnk;

public class Pair implements Comparable<Pair> {

    int score;
    private final Player player;

    public Pair(final Player player) {
        this(0, player);
    }

    public Pair(final int score, final Player player) {
        this.score = score;
        this.player = player;
    }

    public static Pair of(final int score, final Player player) {
        return new Pair(score, player);
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Pair pair = (Pair) o;

        return score == pair.score && player == pair.player;
    }

    @Override
    public String toString() {
        return player.getName() + " ".repeat(30 - player.getName().length()) + "| " + score;
    }

    @Override
    public int compareTo(Pair o) {
        return o.score - score;
    }
}
