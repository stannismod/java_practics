package md2html;

import java.io.*;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

public class Md2Html {

    private static Map<String, String> elements = Map.of(
            "*", "<em>",
            "_", "<em>",
            "**", "<strong>",
            "__", "<strong>",
            "--", "<s>",
            "`", "<code>",
            "++", "<u>"
    );

    private static Map<Character, String> replacements = Map.of(
            '<', "&lt;",
            '>', "&gt;",
            '&', "&amp;"
    );

    private static String parseHeader(String str) {
        if (str.charAt(0) != '#') {
            return "";
        }

        int i = 0;
        while (i <= 6 && i < str.length() && str.charAt(i) == '#') {
            i++;
        }
        if (str.charAt(i) != ' ') {
            return str.substring(0, i);
        }
        return "<h" + i + ">";
    }

    private static boolean checkHeader(String str) {
        if (str.charAt(0) != '#') {
            return false;
        }

        int i;
        for (i = 0; i <= 6 && i < str.length() && str.charAt(i) == '#'; i++);

        return str.charAt(i) == ' ';
    }

    private static int mark(String str, int beginIndex) {
        char start = str.charAt(beginIndex);
        int i;
        for (i = 0; i < 6 && (i + beginIndex) < str.length() && start == str.charAt(i + beginIndex); i++);
        return i + beginIndex;
    }

    private static String getElement(String str, int beginIndex) {
        return elements.get(str.substring(beginIndex, mark(str, beginIndex)));
    }

    private static boolean isElementChar(char c) {
        return c == '+' || c == '*' || c == '_' || c == '`' || c == '-';
    }

    private static boolean print(BufferedWriter writer, Deque<String> elements) {
        try {
            while (elements.size() > 0) {
                writer.write(elements.pollLast());
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Output error: " + e.getMessage());
            return true;
        }
        return false;
    }

    private static boolean shouldIgnoreCharacter(String str, int start, int end) {
        return (start > 0 && str.charAt(start - 1) == '\\') || ((start == 0 || str.charAt(start - 1) == ' ') && (end == str.length() || str.charAt(end) == ' '));
    }

    private static Deque<String> marks;

    private static String processMarks(String str) {
        StringBuilder b = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            if (isElementChar(str.charAt(i))) {
                int start = i;
                int end = mark(str, i);

                // Check if we should not parse this mark
                // Note: we have '\' on left or ' ' on left and right side
                if (shouldIgnoreCharacter(str, start, end)) {
                    b.append(str.charAt(i));
                    continue;
                }

                String elem = getElement(str, start);
                if (!marks.isEmpty() && marks.peek().equals(elem)) {
                    b.append(elem.replace("<", "</"));
                    marks.pop();
                } else if (elem != null) {
                    b.append(elem);
                    marks.push(elem);
                } else {
                    b.append(str.substring(start, end));
                }

                i = end - 1;
            } else {
                if (str.charAt(i) == '\\' && str.charAt(i + 1) != ' ') {
                    continue;
                }
                b.append(str.charAt(i));
            }
        }

        return b.toString();
    }

    private static String processString(String input) {

        StringBuilder b = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            String replacement = replacements.get(input.charAt(i));
            b.append(replacement == null ? input.charAt(i) : replacement);
        }

        input = processMarks(b.toString());

        return input;
    }

    private enum State {
        HEADER,
        PARAGRAPH,
        STOP
    }

    public static void main(String[] args) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(args[1]));
        } catch (IOException e) {
            System.err.println("Cannot create writer for output file: " + e.getMessage());
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            String s;
            Deque<String> queue = new ArrayDeque<>();
            marks = new ArrayDeque<>();
            State state = State.PARAGRAPH;

            while ((s = reader.readLine()) != null && state != State.STOP) {
                if (!s.isEmpty()) {
                    if (checkHeader(s)) {
                        state = State.HEADER;
                        queue.push(parseHeader(s) + processString(s.substring(mark(s, 0) + 1)));
                    } else {
                        state = State.PARAGRAPH;
                        queue.push("<p>" + processString(s));
                    }

                    while ((s = reader.readLine()) != null) {
                        if (s.isEmpty()) {
                            String s1 = queue.getLast();
                            queue.addFirst(queue.pollFirst() + s1.substring(0, state == State.HEADER ? 4 : 3).replace("<", "</"));
                            if (print(writer, queue)) {
                                state = State.STOP;
                            }
                            break;
                        }
                        queue.push(processString(s));
                    }
                }
            }

            if (!queue.isEmpty()) {
                String s1 = queue.getLast();
                queue.addFirst(queue.pollFirst() + s1.substring(0, state == State.HEADER ? 4 : 3).replace("<", "</"));
                print(writer, queue);
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                System.err.println("Cannot close writer: " + e.getMessage());
            }
        }
    }
}