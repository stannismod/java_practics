package search;

public class BinarySearchMissing {

    // Pre: a[i] >= a[i + 1]
    // Post: a[result] = x || (x нет в массиве && он должен быть вставлен на позицию result)
    private static int find(int[] a, int x, boolean recursive) {
        int result;

        if (recursive) {
            // we are recursive
            result = findRecursive(a, -1, a.length, x);
        } else {
            // we are not recursive
            result = findIterative(a, x);
        }

        if (result < a.length && a[result] == x) {
            return result;
        } else {
            return -result - 1;
        }
    }

    // Pre: ∀i a[i] >= a[i + 1]
    // inv: (l;r] contains x && -1 < l < r < a.length
    // Post: (r-1;r] => a[r] = x
    private static int findIterative(int[] a, int x) {
        int l = -1;
        int r = a.length;

        while (r - l > 1) {
            int m = (r + l) / 2;
            // l < m < r
            if (a[m] <= x) {
                // i: a[i] = x && i <= m
                r = m;
            } else {
                // i: a[i] = x && i > m
                l = m;
            }
        }
        // r - l <= 1

        return r;
    }

    // Pre: ∀i a[i] >= a[i + 1]
    // inv: a contains x && -1 < l < r < a.length
    // Post: n = findRecursive(...) => a[n] = x
    private static int findRecursive(int[] a, int l, int r, int x) {
        if (r - l <= 1) {
            return r;
        }

        int m = (l + r) / 2;
        // l < m < r

        if (a[m] <= x) {
            // i: a[i] = x && i <= m
            return findRecursive(a, l, m, x);
        } else {
            // i: a[i] = x && i > m
            return findRecursive(a, m, r, x);
        }
    }

    // Pre: args.length >= 1 - число x должно быть подано, массив может быть пуст
    public static void main(String[] args) {
        int x = Integer.parseInt(args[0]);
        int[] array = new int[args.length - 1];

        for (int i = 1; i < args.length; i++) {
            array[i - 1] = Integer.parseInt(args[i]);
        }

        System.out.println(String.valueOf(find(array, x, true)));
    }
}