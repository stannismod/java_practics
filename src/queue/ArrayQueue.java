package queue;

import java.util.Arrays;
import java.util.Iterator;

// inv: head links to queue start && tail() links to queue end
public class ArrayQueue extends AbstractQueue<ArrayQueue> {

    private int head;
    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[16];
        init();
    }

    public ArrayQueue(int initialCapacity) {
        this.elements = new Object[initialCapacity];
        init();
    }

    // Post: head = elements.length - 1
    private void init() {
        this.head = this.elements.length - 1;
    }

    // Post: 0 <= return < elements.length
    public int tail() {
        return indexOf(size());
    }

    // [-------_____---]   tail = element.length - (element - head);
    //        ^     ^
    //      head  tail
    // [______-------__]   tail = head - element;
    //        ^     ^
    //       tail  head

    // Post: return is the array index of 'element' element
    private int indexOf(int element) {
        if (head >= element) {
            return head - element;
        } else {
            return elements.length - (element - head);
        }
    }

    // Pre: element != null && size' = size
    // inv: 0 <= size() < elements.length &&  0 <= head < elements.length
    // Post: size = size' && element must have the last element in queue
    public void enqueue(Object element) {
        assert element != null;

        ensureCapacity(size() + 1);
        elements[tail()] = element;
        incSize();
        checkBounds();
    }

    // Post: elements.length >= capacity
    private void ensureCapacity(int capacity) {
        int size = elements.length;
        if (capacity > size) {
            elements = Arrays.copyOf(elements, 2 * capacity);
            if (head - size < 0) {
                // if cyclic, move the right part of cycle to the right bound
                System.arraycopy(elements, head, elements, elements.length - (size - head), size - head);
            }
        }
    }

    // Post: 0 <= head < elements.length
    private void checkBounds() {
        if (head < 0) {
            head = elements.length - 1;
        } else if (head >= elements.length) {
            head = 0;
        }
    }

    // Pre: size() > 0 && head' = head
    // inv: 0 <= size() < elements.length &&  0 <= head < elements.length
    // Post: obj = elements[head']
    public Object dequeue() {
        assert size() > 0;

        Object obj = elements[head--];
        decSize();
        checkBounds();
        return obj;
    }

    // Pre: size() > 0 && head' = head
    // inv: 0 <= size() < elements.length &&  0 <= head < elements.length
    // Post: obj = elements[head']
    public Object element() {
        assert size() > 0;

        return elements[head];
    }

    // Post: return is an last element in queue
    public Object peek() {
        assert size() > 0;

        return elements[indexOf(size() - 1)];
    }

    // Pre: element != null && size' = size && head' = head
    // Post: element must be the first element of the queue && size = size' + 1
    // && (head = head' + 1 || (head' = elements.length - 1 && head = 0))
    public void push(Object element) {
        assert element != null;

        ensureCapacity(size() + 1);
        head++;
        incSize();
        checkBounds();
        elements[head] = element;
    }

    // Pre: size() > 0 && size' = size
    // Post: size = size' - 1 && returned element must be deleted from queue
    public Object remove() {
        assert size() > 0;

        Object obj = elements[indexOf(size() - 1)];
        decSize();
        checkBounds();
        return obj;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public ArrayQueue create() {
        return new ArrayQueue();
    }

    public class ArrayIterator implements Iterator<Object> {

        private int index;

        // Post: returns the availability of the next element in queue
        @Override
        public boolean hasNext() {
            return index < size();
        }

        // Pre: hasNext() = true
        // Post: returned value represents the next element in queue
        @Override
        public Object next() {
            assert hasNext();

            return elements[indexOf(index++)];
        }
    }

    @Override
    public Iterator<Object> iterator() {
        return new ArrayIterator();
    }
}
