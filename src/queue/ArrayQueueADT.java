package queue;

import java.util.Arrays;

public class ArrayQueueADT {

    private int size = 0;
    private Object[] elements = new Object[16];
    private int start = elements.length - 1;

    // [-------_____---]   size = element.length - (end - start);
    //        ^     ^
    //      start  end
    // [______-------__]   size = start - end;
    //        ^     ^
    //       end  start

    public static int size(ArrayQueueADT queue) {
        return queue.size;
    }

    public static int tail(ArrayQueueADT queue) {
        return indexOf(queue, queue.size);
    }

    private static int indexOf(ArrayQueueADT queue, int element) {
        if (queue.start >= element) {
            return queue.start - element;
        } else {
            return queue.elements.length - (element - queue.start);
        }
    }

    public static void enqueue(ArrayQueueADT queue, Object element) {
        assert element != null;

        ensureCapacity(queue, size(queue) + 1);
        queue.elements[tail(queue)] = element;
        queue.size++;
        checkBounds(queue);
    }

    private static void ensureCapacity(ArrayQueueADT queue, int capacity) {
        int size = queue.elements.length;
        if (capacity > size) {
            queue.elements = Arrays.copyOf(queue.elements, 2 * capacity);
            if (queue.start - size < 0) {
                // if cyclic, move the right part of cycle to the right bound
                System.arraycopy(queue.elements, queue.start, queue.elements, queue.elements.length - (size - queue.start), size - queue.start);
            }
        }
    }

    private static void checkBounds(ArrayQueueADT queue) {
        if (queue.start < 0) {
            queue.start = queue.elements.length - 1;
        } else if (queue.start >= queue.elements.length) {
            queue.start = 0;
        }
    }

    public static Object dequeue(ArrayQueueADT queue) {
        assert size(queue) > 0;

        Object obj = queue.elements[queue.start--];
        queue.size--;
        checkBounds(queue);
        return obj;
    }

    public static Object element(ArrayQueueADT queue) {
        assert size(queue) > 0;

        return queue.elements[queue.start];
    }

    public static Object peek(ArrayQueueADT queue) {
        assert size(queue) > 0;

        return queue.elements[indexOf(queue,queue.size - 1)];
    }

    public static void push(ArrayQueueADT queue, Object element) {
        assert element != null;

        ensureCapacity(queue,size(queue) + 1);
        queue.start++;
        queue.size++;
        checkBounds(queue);
        queue.elements[queue.start] = element;
    }

    public static Object remove(ArrayQueueADT queue) {
        assert size(queue) > 0;

        Object obj = queue.elements[indexOf(queue,queue.size - 1)];
        queue.size--;
        checkBounds(queue);
        return obj;
    }

    public static boolean isEmpty(ArrayQueueADT queue) {
        return size(queue) == 0;
    }

    public static void clear(ArrayQueueADT queue) {
        queue.size = 0;
    }

    public static String toStr(ArrayQueueADT queue) {
        StringBuilder result = new StringBuilder().append("[");
        for (int i = 0; i < queue.size; i++) {
            result.append(queue.elements[indexOf(queue, i)].toString());
            if (i != queue.size - 1) {
                result.append(", ");
            }
        }
        return result.append("]").toString();
    }

    public static Object[] toArray(ArrayQueueADT queue) {
        Object[] result = new Object[size(queue)];
        int ind = 0;
        while (ind != queue.size) {
            result[ind] = queue.elements[indexOf(queue, ind)];
            ind++;
        }
        return result;
    }
}
