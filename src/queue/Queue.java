package queue;

import java.util.function.Function;
import java.util.function.Predicate;

// T must be a type of final interface realization
public interface Queue<T extends Queue> extends Iterable<Object> {

    // Pre: element != null
    void enqueue(Object element);

    // Pre: size() > 0
    // Post: returns link to removed element
    Object dequeue();

    // returns the number of elements in queue
    int size();

    // Pre: size() > 0
    // Post: returns link to head element in queue
    Object element();

    boolean isEmpty();

    void clear();

    T create();

    // Pre: predicate != null
    // Post: returns the queue filtered by given predicate
    default T filter(Predicate<Object> predicate) {
        T result = create();
        forEach(element -> {
            if (predicate.test(element)) {
                result.enqueue(element);
            }
        });
        return result;
    }

    // Pre: function != null
    // Post: returns the queue applied with given function
    default T map(Function<Object, Object> function) {
        T result = create();
        forEach(element -> result.enqueue(function.apply(element)));
        return result;
    }
}
