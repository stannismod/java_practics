package queue;

import java.util.Arrays;

public class ArrayQueueModule {

    private static int start;
    private static int size = 0;
    private static Object[] elements = new Object[2];

    static {
        start = elements.length - 1;
    }

    // [-------_____---]   size = element.length - (end - start);
    //        ^     ^
    //      start  end
    // [______-------__]   size = start - end;
    //        ^     ^
    //       end  start

    public static int size() {
        return size;
    }

    public static int tail() {
        return indexOf(size);
    }

    private static int indexOf(int element) {
        if (start >= element) {
            return start - element;
        } else {
            return elements.length - (element - start);
        }
    }

    public static void enqueue(Object element) {
        assert element != null;

        ensureCapacity(size() + 1);
        elements[tail()] = element;
        size++;
        checkBounds();
    }

    private static void ensureCapacity(int capacity) {
        int size = elements.length;
        if (capacity > size) {
            elements = Arrays.copyOf(elements, 2 * capacity);
            if (start - size < 0) {
                // if cyclic, move the right part of cycle to the right bound
                System.arraycopy(elements, start, elements, elements.length - (size - start), size - start);
            }
        }
    }

    private static void checkBounds() {
        if (start < 0) {
            start = elements.length - 1;
        } else if (start >= elements.length) {
            start = 0;
        }
    }

    public static Object dequeue() {
        assert size() > 0;

        Object obj = elements[start--];
        size--;
        checkBounds();
        return obj;
    }

    public static Object element() {
        assert size() > 0;

        return elements[start];
    }

    public static Object peek() {
        assert size() > 0;

        return elements[indexOf(size - 1)];
    }

    public static void push(Object element) {
        assert element != null;

        ensureCapacity(size() + 1);
        start++;
        size++;
        checkBounds();
        elements[start] = element;
    }

    public static Object remove() {
        assert size() > 0;

        Object obj = elements[indexOf(size - 1)];
        size--;
        checkBounds();
        return obj;
    }

    public static boolean isEmpty() {
        return size() == 0;
    }

    public static void clear() {
        size = 0;
    }

    public static String toStr() {
        StringBuilder result = new StringBuilder().append("[");
        for (int i = 0; i < size; i++) {
            result.append(elements[indexOf(i)].toString());
            if (i != size - 1) {
                result.append(", ");
            }
        }
        return result.append("]").toString();
    }

    public static Object[] toArray() {
        Object[] result = new Object[size()];
        for (int i = 0; i < size; i++) {
            result[i] = elements[indexOf(i)];
        }
        return result;
    }
}
