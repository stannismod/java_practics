package queue;

public abstract class AbstractQueue<T extends AbstractQueue> implements Queue<T> {

    private int size;

    @Override
    public int size() {
        return size;
    }

    protected void incSize() {
        size++;
    }

    protected void decSize() {
        size--;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
}
