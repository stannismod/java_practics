package queue;

import java.util.Iterator;

public class LinkedQueue extends AbstractQueue<LinkedQueue> {

    private class Node {
        private Object value;
        private Node next;

        private Node(Object value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    private Node head;
    private Node tail;

    @Override
    public void enqueue(Object element) {
        assert element != null;

        Node next = new Node(element, null);
        incSize();
        if (head == null) {
            head = next;
        } else if (tail == null) {
            tail = next;
            head.next = tail;
        } else {
            tail.next = next;
            tail = next;
        }
    }

    @Override
    public Object dequeue() {
        assert size() > 0;

        Node head = this.head;
        this.head = this.head.next;
        if (this.head == null) {
            this.tail = null;
        }
        decSize();
        return head.value;
    }

    @Override
    public Object element() {
        if (isEmpty()) {
            return null;
        }
        return head.value;
    }

    @Override
    public void clear() {
        super.clear();
        this.head = null;
    }

    @Override
    public LinkedQueue create() {
        return new LinkedQueue();
    }

    public class LinkedIterator implements Iterator<Object> {

        private LinkedQueue.Node current = head;

        // Post: returns the availability of the next element in queue
        @Override
        public boolean hasNext() {
            return current != null;
        }

        // Pre: hasNext() = true
        // Post: returned value represents the next element in queue
        @Override
        public Object next() {
            assert hasNext();

            Object value = current.value;
            current = current.next;
            return value;
        }
    }

    @Override
    public Iterator<Object> iterator() {
        return new LinkedIterator();
    }
}
