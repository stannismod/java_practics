package markup;

import java.util.List;

public abstract class TextElementContainerWrapper extends TextElementContainer {

    public TextElementContainerWrapper(List<TextElement> elements) {
        super(elements);
    }

    @Override
    public void toMarkdown(StringBuilder builder) {
        builder.append(getMark());
        super.toMarkdown(builder);
        builder.append(getMark());
    }

    @Override
    public void toHtml(StringBuilder builder) {
        builder.append("<").append(getHtmlTag()).append(">");
        super.toHtml(builder);
        builder.append("</").append(getHtmlTag()).append(">");
    }

    protected abstract String getMark();

    protected abstract String getHtmlTag();
}
