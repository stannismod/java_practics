package markup;

public class Text implements TextElement {

    protected String str;

    public Text(String str) {
        this.str = str;
    }

    @Override
    public void toMarkdown(StringBuilder builder) {
        builder.append(str);
    }

    @Override
    public void toHtml(StringBuilder builder) {
        toMarkdown(builder);
    }

    int length() {
        return str.length();
    }

    char charAt(int index) {
        return str.charAt(index);
    }

    String getStr() {
        return str;
    }
}
