package markup;

import java.util.List;

public class Strong extends TextElementContainerWrapper {

    public Strong(List<TextElement> elements) {
        super(elements);
    }

    @Override
    public String getMark() {
        return "__";
    }

    @Override
    public String getHtmlTag() {
        return "strong";
    }
}
