package markup;

import java.util.ArrayList;

public class Header extends TextElementContainerWrapper implements TextElement {

    private int level;

    public Header(String text, int level) {
        super(new ArrayList<>());
        this.add(new Text(text));
        this.level = level;
    }

    @Override
    public void toMarkdown(StringBuilder builder) {
        builder.append(getMark());
        super.toMarkdown(builder);
    }

    @Override
    public void toHtml(StringBuilder builder) {
        builder.append('<').append(getHtmlTag()).append('>');
        super.toHtml(builder);
        builder.append("</").append(getHtmlTag()).append('>');
    }

    @Override
    protected String getMark() {
        StringBuilder mark = new StringBuilder();
        for (int i = 0; i < level; i++) {
            mark.append('#');
        }
        return mark.toString();
    }

    @Override
    protected String getHtmlTag() {
        return "h" + level;
    }

    public void append(String str) {
        this.add(new Text(str));
    }
}
