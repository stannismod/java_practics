package markup;

import java.util.List;

public class Paragraph extends TextElementContainerWrapper {

    public Paragraph(List<TextElement> elements) {
        super(elements);
    }

    @Override
    protected String getMark() {
        return "";
    }

    @Override
    protected String getHtmlTag() {
        return "p";
    }

}
