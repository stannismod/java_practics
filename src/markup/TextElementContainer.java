package markup;

import java.util.List;

public class TextElementContainer implements TextElement {

    protected List<TextElement> elements;

    public TextElementContainer(List<TextElement> elements) {
        this.elements = elements;
    }

    public void add(TextElement element) {
        elements.add(element);
    }

    public TextElement getElement(int index) {
        return elements.get(index);
    }

    @Override
    public void toMarkdown(StringBuilder builder) {
        for (TextElement element : elements) {
            element.toMarkdown(builder);
        }
    }

    @Override
    public void toHtml(StringBuilder builder) {
        for (TextElement element : elements) {
            element.toHtml(builder);
        }
    }
}
