package markup;

import java.util.List;

public class Code extends TextElementContainerWrapper {

    public Code(List<TextElement> elements) {
        super(elements);
    }

    @Override
    protected String getMark() {
        return "`";
    }

    @Override
    protected String getHtmlTag() {
        return "code";
    }
}
