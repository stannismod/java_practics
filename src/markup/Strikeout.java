package markup;

import java.util.List;

public class Strikeout extends TextElementContainerWrapper {

    public Strikeout(List<TextElement> elements) {
        super(elements);
    }

    @Override
    protected String getMark() {
        return "~";
    }

    @Override
    protected String getHtmlTag() {
        return "s";
    }
}
