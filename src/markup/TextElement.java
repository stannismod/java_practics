package markup;

public interface TextElement {

    void toMarkdown(StringBuilder builder);

    void toHtml(StringBuilder builder);

    default boolean isSame(TextElement other) {
        return this.getClass() == other.getClass();
    }
}
