package markup;

import java.util.List;

public class Emphasis extends TextElementContainerWrapper {

    public Emphasis(List<TextElement> elements) {
        super(elements);
    }

    @Override
    public String getMark() {
        return "*";
    }

    @Override
    public String getHtmlTag() {
        return "em";
    }
}
