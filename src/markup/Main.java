package markup;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Paragraph paragraph = new Paragraph(List.of(
                new Strong(List.of(
                        new Text("1"),
                        new Strikeout(List.of(
                                new Text("2"),
                                new Emphasis(List.of(
                                        new Text("3"),
                                        new Text("4")
                                )),
                                new Text("5")
                        )),
                        new Text("6")
                ))
        ));
        StringBuilder b = new StringBuilder();
        paragraph.toMarkdown(b);
        StringBuilder b1 = new StringBuilder();
        paragraph.toHtml(b1);
        System.out.println(b.toString());
        System.out.println(b1.toString());
    }
}
