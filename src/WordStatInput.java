import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WordStatInput {

    private static boolean isWordPart(char c) {
        return Character.isLetter(c) || Character.getType(c) == Character.DASH_PUNCTUATION || c == '\'';
    }

    private static List<String> split(String input) {
        StringBuilder b = new StringBuilder();
        List<String> list = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (isWordPart(c)) {
                b.append(input.charAt(i));
            }
            if (b.length() > 0 && (!isWordPart(c) || i == input.length() - 1)) {
                list.add(b.toString().toLowerCase());
                b.setLength(0);
            }
        }

        return list;
    }

    public static void main(String[] args) {
        LinkedHashMap<String, Integer> stat = new LinkedHashMap<>();
        String s;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(args[0])), "utf8"));
            try {
                while ((s = reader.readLine()) != null) {
                    List<String> split = split(s);
                    for (String word : split) {
                        if (stat.containsKey(word)) {
                            int count = stat.get(word) + 1;
                            stat.put(word, count);
                        } else {
                            stat.put(word, 1);
                        }
                    }
                }
            } finally {
                reader.close();
            }

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(args[1])), "utf8"));
            try {
                for (Map.Entry<String, Integer> entry : stat.entrySet()) {
                    writer.write(entry.getKey() + " " + entry.getValue() + "\n");
                }
            } finally {
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
