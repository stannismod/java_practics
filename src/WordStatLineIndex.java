import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WordStatLineIndex {

    private static boolean isWordPart(char c) {
        return Character.isLetter(c) || Character.getType(c) == Character.DASH_PUNCTUATION || c == '\'';
    }

    private static List<String> split(String input) {
        List<String> list = new ArrayList<>();
        int start;

        for (int i = 0; i < input.length(); i++) {
            while (i < input.length() && !isWordPart(input.charAt(i))) {
                i++;
            }

            start = i;

            while (i < input.length() && isWordPart(input.charAt(i))) {
                i++;
            }
            if (i != start) {
                list.add(input.substring(start, i).toLowerCase());
            }
        }

        return list;
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            return;
        }

        Map<String, List<Integer>> stat = new TreeMap<>();
        int counterLine = 1;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(args[0])), "utf8"))) {
            String s;
            while ((s = reader.readLine()) != null) {
                int counterRow = 1;
                List<String> split = split(s);
                for (String word : split) {
                    List<Integer> list = stat.get(word);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(counterLine);
                    list.add(counterRow++);
                    stat.put(word, list);
                }
                counterLine++;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Cannot find the input file " + args[0]);
        } catch (IOException e) {
            System.err.println("Error while reading from input file " + args[0] + ": " + e.getMessage());
            e.printStackTrace();
        }

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(args[1])), "utf8"))) {
            for (Map.Entry<String, List<Integer>> entry : stat.entrySet()) {
                List<Integer> list = entry.getValue();
                writer.write(entry.getKey() + " " + list.size() / 2);
                for (int i = 0; i < list.size(); i += 2) {
                    writer.write(" " + list.get(i) + ":" + list.get(i + 1));
                }
                writer.newLine();
            }
        } catch (IOException e) {
            System.err.println("Error while writing to output file " + args[1] + ":");
            e.printStackTrace();
        }
    }
}
