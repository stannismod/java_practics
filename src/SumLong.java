import java.util.ArrayList;
import java.util.List;

public class SumLong {
	
	public static List<String> split(String input) {
		StringBuilder b = new StringBuilder();
		List<String> list = new ArrayList<>();
		
		for (int i = 0; i < input.length(); i++) {
			if (!Character.isWhitespace(input.charAt(i))) {
				b.append(input.charAt(i));
			}
			if (b.length() > 0 && (Character.isWhitespace(input.charAt(i)) || i == input.length() - 1)) {
				list.add(b.toString());
				b.setLength(0);
			}
		}

		return list;
	}
	
	public static void main(String[] args) {
		long sum = 0;
		for (String arg : args) {
			List<String> subArgs = split(arg);
			for (String subArg : subArgs) {
				sum += Long.parseLong(subArg);
			}
		}
		System.out.println(sum);
	}
}