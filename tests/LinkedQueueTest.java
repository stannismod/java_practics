import org.junit.Assert;
import org.junit.Test;
import queue.LinkedQueue;
import queue.Queue;

import java.util.ArrayDeque;
import java.util.Random;

public class LinkedQueueTest extends Assert {

    @Test
    public void testSequential() {
        Queue queue = new LinkedQueue();
        queue.enqueue(3);
        assertEquals(1, queue.size());
        queue.enqueue(4);
        assertEquals(2, queue.size());
        queue.enqueue(5);
        assertEquals(3, queue.size());
        queue.enqueue(6);
        assertEquals(4, queue.size());
        queue.enqueue(7);
        assertEquals(5, queue.size());
        for (int i = 0; i < 5; i++) {
            System.out.println(queue.dequeue());
        }

        queue.enqueue(1);
        queue.enqueue(2);
        assertEquals(1, queue.dequeue());
        queue.enqueue(3);
        assertEquals(2, queue.dequeue());
        queue.enqueue(10);
        System.out.println("TestSequential passed successful");
    }

    @Test
    public void testRandom() {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        Queue queue = new LinkedQueue();
        Random rand = new Random();
        for (int i = 0; i < 1000; i++) {
            int num = rand.nextInt();
            deque.push(num);
            queue.enqueue(num);
        }
        for (int i = 0; i < 1000; i++) {
            assertEquals(deque.pollLast(), queue.dequeue());
        }
        System.out.println("TestRandom passed successful");
    }
}