import org.junit.Assert;
import org.junit.Test;
import queue.ArrayQueueModule;

import java.util.ArrayDeque;
import java.util.Random;

public class ArrayQueueTest extends Assert {

    @Test
    public void testSequential() {
        ArrayQueueModule queue = new ArrayQueueModule();
        queue.enqueue(3);
        assertEquals(1, queue.size());
        queue.enqueue(4);
        assertEquals(2, queue.size());
        queue.enqueue(5);
        assertEquals(3, queue.size());
        queue.enqueue(6);
        assertEquals(4, queue.size());
        queue.enqueue(7);
        assertEquals(5, queue.size());
        System.out.println(queue.toStr());
        for (int i = 0; i < 5; i++) {
            System.out.println(queue.dequeue());
        }

        queue.enqueue(1);
        queue.enqueue(2);
        assertEquals(1, queue.dequeue());
        queue.enqueue(3);
        assertEquals(2, queue.dequeue());
        queue.enqueue(10);
        System.out.println(queue.toStr());
        System.out.println("TestSequential passed successful");
    }

    @Test
    public void testRandom() {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        ArrayQueueModule queue = new ArrayQueueModule();
        Random rand = new Random();
        for (int i = 0; i < 50; i++) {
            int num = rand.nextInt();
            deque.push(num);
            queue.enqueue(num);
        }
        queue.toArray();
        for (int i = 0; i < 50; i++) {
            assertEquals(deque.pollLast(), queue.dequeue());
        }
        System.out.println("TestRandom passed successful");
    }
}
